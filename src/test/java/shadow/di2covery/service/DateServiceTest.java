package shadow.di2covery.service;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Date;

public class DateServiceTest extends TestCase {
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public DateServiceTest(final String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(DateServiceTest.class);
    }

    public void testHelloDateService(){
        DateService.helloDateService();
    }

    public void testCompareDateWithMethode_getTime(){
        DateService.compareDateWithMethod_getTime();
    }

    public void testCompareDateWithMethode_compareTo(){
        DateService.compareDateWithMethod_compareTo();
    }

    public void testFormatDate(){
        Date toDay = new Date();
        String format = "E yyyy.MM.dd 'at' hh:mm:ss a zzz";

        String result = DateService.formatDate(toDay, format);
        System.out.println("Format date : from \n\t" + toDay + "\n to \n\t" + result);
    }

    public void testParseDate(){
        String date = "2020-12-18";
        String format = "yyyy-MM-dd";

        Date result = DateService.parseDate(date, format);
        System.out.println("Parse date : from 2020-12-18 to " + result);
    }

}
