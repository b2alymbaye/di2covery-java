package shadow.di2covery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Di2coveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(Di2coveryApplication.class, args);
    }

}
