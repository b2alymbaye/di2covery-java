package shadow.di2covery.service;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public abstract class DateService {

    public static void helloDateService(){
        System.out.println("Hello from DateService.");
    }

    /**********************************
     * Ways to compare two dates.
     **********************************/

    public static void compareDateWithMethod_getTime(){
        Date toDay = new Date();

        Date tomorrow = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(tomorrow);
        c.add(Calendar.DATE, 1);
        tomorrow = c.getTime();

        boolean flag = toDay.getTime() == tomorrow.getTime();

        System.out.println("toDay == tomorrow ?: " + flag);
    }

    public static void compareDateWithMethod_compareTo(){
        Date toDay = new Date();

        Date tomorrow = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(tomorrow);
        c.add(Calendar.DATE, 1);
        tomorrow = c.getTime();

        // -1 if today is < tomorrow
        // 0 if today is = tomorrow
        // 1 if today is > tomorrow
        System.out.println(toDay.compareTo(tomorrow));
    }

    /**********************************
     * Date formating.
     **********************************/

    public static String formatDate(final Date date, final String format){   // format : we start from Date to end with String.
        SimpleDateFormat ft = new SimpleDateFormat (format);
        return ft.format(date);
    }

    /**********************************
     * Date parsing.
     **********************************/

    public static Date parseDate(final String date, final String format) {   // parse : we start from String to end with Date.
        try {
            SimpleDateFormat ft = new SimpleDateFormat(format);

            return ft.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }


}
